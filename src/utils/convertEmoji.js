const emoji = require('node-emoji')

function getEmoji(match) {
  const got = emoji.get(match) || match;
  return got;
}

export const convertEmoji = (item) => {
  return getEmoji(item)
}
