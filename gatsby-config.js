const remarkSlug = require(`remark-slug`)
const remarkEmoji = require('remark-emoji')
const squeezeParagraphs = require('remark-squeeze-paragraphs')
const unwrapImages = require(`remark-unwrap-images`)
const TextCleaner = require(`text-cleaner`)

const clean = (string) => {
  return TextCleaner(string)
    .removeChars({ exclude: `/`, replaceWith: ` ` })
    .removeStopWords()
    .stripHtml()
    .condense()
    .toLowerCase()
    .valueOf()
}

module.exports = {
  pathPrefix: `/notes`,
  siteMetadata: {
    title: `Jegyzet`,
    description: `ELTE IK Egyetemi jegyzet`,
    gitRepoContentPath: ``, // does not work for submodules :c , so leave it blank
    showDescriptionInSidebar: true,
    logo: ``, // does not work unless from an internet link (i may need to fiddle with the gatsby-filesystem)
    openSearch: {
      siteShortName: `Jegyzetek`,
      siteUrl: 'https://pitato.gitlab.io/notes',
      siteDescription: 'ELTE IK egyetemi jegyzetek',
    }
  },
  plugins: [
    `gatsby-plugin-sharp`,
    `gatsby-transformer-sharp`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `${__dirname}/notes`,
        path: `${__dirname}/notes`,
      }
    },
    {
      resolve: `gatsby-plugin-mdx`,
      options: {
        extensions: [`.md`, `.mdx`],
        gatsbyRemarkPlugins: [
          {
            resolve: `gatsby-remark-images`,
            options: {
              backgroundColor: `none`,
              maxWidth: 900,
              disableBgImage: true,
              wrapperStyle: `margin: 1.5rem 0;`
            }
          },
          {
            resolve: `gatsby-remark-katex`,
            options: {
              strict: `ignore`,
              output: `html`
            }
          },
          {
            resolve: `gatsby-remark-embed-snippet`,
            options: {},
          },
          `gatsby-remark-embedder`
        ],
        remarkPlugins: [
          remarkSlug,
          remarkEmoji,
          squeezeParagraphs,
          unwrapImages
        ]
      }
    },
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-theme-ui`,
    {
      resolve: `gatsby-plugin-local-search`,
      options: {
        name: `notes`,
        engine: `flexsearch`,
        engineOptions: {
          encode: `icase`,
          tokenize: `forward`,
          resolution: 9
        },
        query: `{
          allNotes: allMdx {
            edges {
              node {
                id
                frontmatter {
                  title
                  emoji
                  tags
                }
                fields {
                  slug
                }
                rawBody
              }
            }
          }
        }`,
        ref: `id`,
        index: [`title`, `body`, `tagsJoint`],
        store: [`id`, `slug`, `title`, `body`, `tags`, `emoji`],
        normalizer: ({ data }) =>
          data.allNotes.edges.map(({ node }) => {
            return {
              id: node.id,
              slug: node.fields.slug,
              title: node.frontmatter.title,
              body: clean(node.rawBody),
              emoji: node.frontmatter.emoji,
              tags: node.frontmatter.tags,
              tagsJoint:
                node.frontmatter.tags &&
                node.frontmatter.tags.join().replace(/,/gi, ` `)
            }
          })
      }
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Jegyzetek`,
        short_name: `Jegyzet`,
        description: `ELTE IK egyetemi jegyzetek.`,
        start_url: `/`,
        background_color: `hsl(210, 38%, 95%)`,
        theme_color: `hsl(345, 100%, 69%)`,
        display: `standalone`,
        icon: `src/static/logo.png`
      }
    },
    `gatsby-plugin-catch-links`
  ]
}
